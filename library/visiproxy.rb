# encoding: utf-8

require 'thor'
require 'majic'
require 'bundler'
require 'eventmachine'
require 'em-http-request'

require 'visiproxy/cli'
require 'visiproxy/proxy'
require 'visiproxy/scanner'

Bundler.setup :default

module VisiProxy
  Version = "0.1"
end
