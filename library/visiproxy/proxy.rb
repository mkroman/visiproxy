# encoding: utf-8

module VisiProxy
  class Proxy
    DefaultStatus = "Untested"

    attr_accessor :host, :port, :tested, :working, :status

    def self.parse line
      if line =~ /([\w\.]+):(\d+)/
        Proxy.new $1, $2.to_i
      end
    end

    def initialize host, port = 3128
      @host = host
      @port = port
      @status = DefaultStatus
    end

    def tested?
      @tested
    end

    def working?
      @tested and @working
    end

    def tested! working, status
      @working = working
      @status = status
    end
  end
end