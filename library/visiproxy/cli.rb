# encoding: utf-8

module VisiProxy
  class CLI < Thor
    DefaultFilePath = "Proxies.txt"

    desc "scan [file]", "Scan a list of proxies."
    option :username, aliases: [:user, :u], required: true
    option :password, aliases: [:pass, :p], required: true
    option :concurrent, aliases: [:count, :c], default: 10, type: :numeric

    def scan file = DefaultFilePath
      path = File.join File.dirname($0), file

      unless File.exists? path
        error "Could not open proxy list"

        return
      end

      @scanner = Scanner.new options[:username], options[:password]
      @scanner.concurrent = options[:concurrent]
      @scanner.load path
      @scanner.start
    end
  end
end