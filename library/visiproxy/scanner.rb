# encoding: utf-8

module VisiProxy
  class Scanner
    LoginURL = "http://n.dk/community/login/login.asp"

    attr_accessor :concurrent

    def initialize username, password
      @index = 0
      @proxies = []
      @cookies = []
      @username = username
      @password = password
      @concurrent = 10
    end

    def finished?
      @proxies.find{ |proxy| not proxy.tested? }
    end

    def load path
      File.open path do |file|
        file.each_line do |line| 
          proxy = Proxy.parse line
          @proxies << proxy if proxy
        end
      end
    end

    def create_request proxy
      options = {
        :proxy => {
          :host => proxy.host,
          :port => proxy.port
        },

        :connect_timeout => 30
      }

      EventMachine::HttpRequest.new LoginURL, options
    end

    def proxy_request
      return if @index == @proxies.length

      head = { cookie: @cookies }
      body = { username: @username, pdummy: :kode, password: @password }
      
      proxy = @proxies[@index]
      request = create_request(proxy).post body: body, head: head

      request.callback do
        if request.response.include? "Ikke dansk IP-nummer. Venligst kontakt support"
          proxy.tested! false, "International IP-address"
        elsif request.response.include? "this.location.href='SessionLoader.aspx?guid="
          proxy.tested! true, "Authenticated and working"
        else
          proxy.tested! false, "Unknown error"
        end

        proxy_tested proxy
      end

      request.errback do
        proxy.tested! false, "Request error"
        proxy_tested proxy
      end

      @index += 1
    end

    def proxy_tested proxy
      uri = "#{proxy.host}:#{proxy.port}".ljust(20) ^ (proxy.working? ? :green : :red)
      puts "  #{uri} — #{proxy.status}…"

      if @index == @proxies.length
        if EventMachine.connection_count == 1
          EventMachine.stop
        end
      else
        proxy_request
      end
    end

    def start
      EventMachine.run do
        puts "Requesting session cookie (UNPROXIED!)…"

        request = EventMachine::HttpRequest.new("http://n.dk/login.aspx").get
        request.headers do |hash|
          if @cookies = hash["SET_COOKIE"]
            puts "Received session cookie, starting to scan…"

            @concurrent.times { proxy_request }
          end
        end
      end
    end
  end
end